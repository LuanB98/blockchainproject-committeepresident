pragma solidity ^0.4.17;

contract CommitteePresidentElection {
    address public CurrentPresident;
    address[] public Nominations;
    address public CommitteFundsAddress;

    function CommitteePresidentElection() public {
        CurrentPresident = msg.sender;
    }

    function SetCommitteeFunds() public {
        CommitteFundsAddress = msg.sender;

    }

    function Nomination() public payable {
        require(msg.value > 1 ether);

        Nominations.push(msg.sender);
    }

    function random() private view returns (uint) {
        return uint(keccak256(block.difficulty, now, Nominations));
    }

    function ElectNewPresident() public restricted {
        uint index = random() % Nominations.length;
        Nominations[index].transfer(this.balance);
        CurrentPresident = Nominations[index];
        Nominations = new address[](0);
    }

    modifier restricted() {
        require(msg.sender == CurrentPresident);
        _;
    }

    function getNominations() public view returns (address[]) {
        return Nomination;
    }

    function getCurrentPresident() public view returns (address){
        return CurrentPresident;

    }

    function getCommitteeFundsAddress() pubic view returns (address) {
      return

    }



}
